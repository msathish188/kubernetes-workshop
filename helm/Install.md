Install HELM and run test application
----------------------------------------
```
wget https://get.helm.sh/helm-v3.3.1-linux-amd64.tar.gz
tar zxf helm-v3.3.1-linux-amd64.tar.gz
sudo mv linux-amd64/helm /usr/local/bin
helm version
helm repo add stable https://kubernetes-charts.storage.googleapis.com/
helm install demo-mysql stable/mysql
Helm 2
helm init
kubectl get all --selector=app=demo-mysql
kubectl get secret --selector=app=demo-mysql
helm uninstall demo-mysql
helm env
```